#!/usr/bin/python

####################################################################
####################################################################
##          Downloadscript für die aktuelle JungleWorld           ##
####################################################################

####################################################################
#~ 
#~ Rev: 19-11-12
#~ 
#~ <Beta>
#~ 
#~ Dependencies: python, firefox + selenium
#~ 
#~ Usage: 
#~         Script und config.ini in einen beliebigen Ordner kopieren. 
#~         In config.ini Login, Passwort und Downloadordner hinterlegen.
#~         (unix/linux:) Script ausführbar machen [chmod +x JW_sel.py].
#~         Script in der Konsole/Powershell ausführen.
#~         Nach belieben zur automatisierung in Autostart/Cron einbinden. 
#~ 
#~ ToDo: 
#~         Chromedriver implementieren
#~         
####################################################################

def main():
    print("This Script downloads the current issue of the Jungle World")
    print("Current Download-Dir:", downPath)

    before = os.listdir(downPath)

    firefox_profile = webdriver.FirefoxProfile()
    firefox_profile.set_preference("browser.privatebrowsing.autostart", True)
    firefox_profile.set_preference("browser.download.folderList", 2)
    firefox_profile.set_preference("browser.download.manager.showWhenStarting", False)
    firefox_profile.set_preference("browser.download.dir", downPath)
    firefox_profile.set_preference("browser.helperApps.neverAsk.saveToDisk", "application/pdf,application/x-pdf")
    firefox_profile.set_preference("pdfjs.disabled", True)

    driver = webdriver.Firefox(firefox_profile=firefox_profile)
    driver.get('https://jungle.world/user/login')

    username = driver.find_element_by_id("edit-name")
    password = driver.find_element_by_id("edit-pass")

    username.send_keys(userName)
    password.send_keys(PassW)

    driver.find_element_by_id("edit-submit").click()

    element = WebDriverWait(driver, 10).until(
            EC.visibility_of_element_located((By.LINK_TEXT, "PDF"))
    )

    time.sleep(2)

    vol = driver.find_elements_by_xpath('//*[@id="block-views-block-titelseite-banner"]/div/div/div/div/article/div/a[1]')
    for voll in vol: 
        vollink = voll.get_attribute("href")
    volno = vollink.split("/")[-2:]
    print("Volume:", volno[0], volno[1])
    basenew = "jungleworld_{0}-{1}.pdf".format(volno[0], volno[1])

    pdfbutton = driver.find_elements_by_xpath('//*[@id="block-views-block-titelseite-banner"]/div/div/div/div/article/div/a[2]')
    for pdfurl in pdfbutton: 
        pdflink = pdfurl.get_attribute("href")
    pdfbase = os.path.basename(pdflink)
    print("Link:", pdflink)
    
    print("PDF-Basename:", pdfbase)
    
    print("Rename after Download:", basenew)

    if basenew in before:
        print("already downloaded")
        driver.quit()
    else:
        driver.find_element_by_xpath('//*[@id="block-views-block-titelseite-banner"]/div/div/div/div/article/div/a[2]').click()  

        time.sleep(7)
        driver.quit()
        
        after = os.listdir(downPath)
        change = set(after) - set(before)
        if len(change) == 1:
            file_name = change.pop()
            print("New File:", file_name)
            print("Renaming")
            os.rename(downPath+file_name, downPath+basenew)
        else:
            print("ERROR: More than one file or no file downloaded")
    
if __name__ == "__main__":
    from selenium import webdriver
    from selenium.webdriver.common.by import By
    from selenium.webdriver.support.ui import WebDriverWait
    from selenium.webdriver.support import expected_conditions as EC
    import configparser
    import time
    import sys
    import os
    
    config = configparser.ConfigParser()
    config.read('config.ini')
    
    downPath = config.get('DownloadDirectory', 'downPath')
    userName = config.get('Credentials', 'userName')
    PassW = config.get('Credentials', 'PassWord')
    
    main()
