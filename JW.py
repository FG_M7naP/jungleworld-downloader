#!/usr/bin/python

import requests
from urllib.parse import urljoin
from bs4 import BeautifulSoup
import configparser
from pathlib import Path
import datetime


def main():
    now = datetime.datetime.now()
    print(f"""
------------{now.strftime('%Y-%m-%dT%H:%M:%S')}------------
This Script downloads the current issue of the Jungle World
    """)

    url_jw_lgi = 'https://jungle.world/user/login'
    url_jw = 'https://jungle.world/'

    s = requests.Session()
    s.post(url_jw_lgi, data={'name': userName, 'pass': PassW, 'form_id': 'user_login_form', 'op': 'Anmelden'})

    r = s.get(url_jw)
    soup = BeautifulSoup(r.text, 'html.parser')

    link = soup.find('a', title='PDF der Ausgabe')
    downlink = link.get('href')
    downlink = urljoin(url_jw, downlink)

    link = soup.find('a', title='Zum Inhalt')
    jw_nr = link.get('href')
    volno = jw_nr.split("/")[-2:]
    downbasename = f"jungleworld_{volno[0]}-{volno[1]}.pdf"

    print(f"""
Downloading {downlink}
and
save as {downbasename}
in
{downPath}
    """)

    try:
        p = downPath / downbasename
        with p.open("xb") as f:
            f.write(s.get(downlink).content)
    except FileExistsError:
        print(f"{downbasename} allready exists")

    print(f"""
=============================================================
""")


if __name__ == "__main__":

    # Common DIRs; uncomment for debug if your bash/shell get wrong DIRs
    cwd = Path.cwd()
    # print("Current Working DIR:", cwd)
    sdir = Path(__file__).resolve()
    wdir = sdir.parent
    # print("Script Name:", __file__)
    # print("Script DIR:", wdir)
    config_file = wdir / "config.ini"
    # print("Config:", config_file)

    config = configparser.ConfigParser()
    config.read(config_file)

    downPath = config.get('DownloadDirectory', 'downPath')
    downPath = Path(downPath)
    userName = config.get('Credentials', 'userName')
    PassW = config.get('Credentials', 'PassWord')

    main()
