#!/usr/bin/python

from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
import time
import subprocess
import sys
import os
import tkinter as tk

#TODO configparser
downPath = "down-path"
userName = "login"
PassW = "pass"
PDFviewer = "/usr/bin/okular"

class MainApp(tk.Frame):
    def __init__(self, parent, *args, **kwargs):
        tk.Frame.__init__(self, parent, *args, **kwargs)
        self.parent = parent


def main():
    root = tk.Tk()
    app = MainApp(root).pack(side="top", fill="both", expand=True)
    root.mailoop()

if __name__ == '__main__':
    main()


def selenDriver():
    print("This Script Download the PDF from the JungleWorld Server")
    print("Current Download-Dir:", downPath)

    before = os.listdir(downPath)

    firefox_profile = webdriver.FirefoxProfile()
    firefox_profile.set_preference("browser.privatebrowsing.autostart", True)
    firefox_profile.set_preference("browser.download.folderList", 2)
    firefox_profile.set_preference("browser.download.manager.showWhenStarting", False)
    firefox_profile.set_preference("browser.download.dir", downPath)
    firefox_profile.set_preference("browser.helperApps.neverAsk.saveToDisk", "application/pdf,application/x-pdf")
    firefox_profile.set_preference("pdfjs.disabled", True)

    driver = webdriver.Firefox(firefox_profile=firefox_profile)
    driver.get('https://jungle.world/user/login')

    username = driver.find_element_by_id("edit-name")
    password = driver.find_element_by_id("edit-pass")

    username.send_keys(userName)
    password.send_keys(PassW)

    driver.find_element_by_id("edit-submit").click()

    element = WebDriverWait(driver, 10).until(
            EC.visibility_of_element_located((By.LINK_TEXT, "PDF"))
    )

    time.sleep(2)

    for a in driver.find_elements_by_xpath('//*[@id="block-views-block-titelseite-banner"]/div/div/div/div/article/div/a[2]'):
        urllink = a.get_attribute("href")
        urlbase = os.path.basename(urllink)
        print("Link:")
        print(urllink)
        print("Basename:")
        print(urlbase)

    if urlbase in before:
        print(urlbase, "already downloaded")
        driver.quit()
        #chooseGUI()
    else:
        driver.find_element_by_xpath('//*[@id="block-views-block-titelseite-banner"]/div/div/div/div/article/div/a[2]').click() 

        time.sleep(7)
        driver.quit()

        after = os.listdir(downPath)
        change = set(after) - set(before)
        if len(change) == 1:
            file_name = change.pop()
            print("New File:", file_name)
            # print("Open", os.path.join(downPath, file_name), "extern?")
            # p = subprocess.Popen([PDFviewer, os.path.join(downPath, file_name)])
            # returncode = p.wait() # wait for exit
        else:
            print("ERROR: More than one file or no file downloaded")

def chooseGUI():
    app = QtWidgets.QApplication(sys.argv)
    window = QtWidgets.QWidget()
    label = QtWidgets.QLabel("Open?")
    buttonY = QtWidgets.QPushButton("Yes")
    buttonN = QtWidgets.QPushButton("No")
    buttonY.clicked.connect(buttonY_clicked)
    buttonN.clicked.connect(buttonN_clicked)
    
    layout = QtWidgets.QVBoxLayout()
    layout.addWidget(label)
    layout.addWidget(buttonY)
    layout.addWidget(buttonN)
    
    window.setLayout(layout)
    window.setWindowTitle("Open file?")
    window.show()
    sys.exit(app.exec_())
 
def buttonY_clicked(): 
    p = subprocess.Popen([PDFviewer, os.path.join(downPath, urlbase)])
    returncode = p.wait() # wait for exit
    window.close()
def buttonN_clicked(): 
    print("Noting to do")
    window.close()
    



