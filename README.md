**Status:** Beta

**Dependencies:** python (with BeautifulSoup, urllib, requests, configparser, pathlib) 

**Usage:**

`JW.py` und `config.ini` in einen beliebigen Ordner kopieren.   
In config.ini Login, Passwort und Downloadordner hinterlegen.  
*unix/linux:* script ausführbar machen (`chmod +x JW.py`).  
Script in der Konsole/Powershell ausführen.  
Nach belieben zur automatisierung in Autostart/Cron einbinden.  

**ToDo:** Automatische Archivierung

--------

Die Ordner `dev_fetch` und `dev_gui` beinhalten noch nicht fertig entwickelte Erweiterungen. 

Der Ordner `old_with-selenium` beinhaltet eine grafische Variante über Firefox und Selenium. 