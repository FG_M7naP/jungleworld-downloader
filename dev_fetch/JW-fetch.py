#!/usr/bin/python

downPath = "down-path"
userName = "login"
PassW = "path"

Y = ["2017", "2018", "2019"]

def main():
    print("This Script Download fetch the PDF from the JungleWorld Server")
    print("Current Download-Dir:", downPath)

    options = webdriver.ChromeOptions()
    profile = {
            "plugins.plugins_list": [{"enabled": False, "name": "Chromium PDF Viewer"}], 
            "download.default_directory": downPath , 
            "download.extensions_to_open": "applications/pdf",
            "plugins.always_open_pdf_externally": True,
            "download.prompt_for_download": False,
            "safebrowsing.enabled": True
            }
    options.add_experimental_option("prefs", profile)
    driver = webdriver.Chrome(options=options)
    
    driver.get('https://jungle.world/user/login')

    username = driver.find_element_by_id("edit-name")
    password = driver.find_element_by_id("edit-pass")

    username.send_keys(userName)
    password.send_keys(PassW)

    driver.find_element_by_id("edit-submit").click()

    time.sleep(4)

    for y in Y:
        print(y)
        for x in range(53):
            url1 = "https://jungle.world/system/files/ausgaben/{0}/jungleworld_{0}-{1:02d}.pdf".format(y, x)
            url2 = "https://jungle.world/system/files/ausgaben/{0}/jungleworld-{0}-{1:02d}.pdf".format(y, x)
            url3 = "https://jungle.world/system/files/ausgaben/{0}/jungleworld-{0}_{1:02d}.pdf".format(y, x)
            url4 = "https://jungle.world/system/files/ausgaben/{0}/jungleworld_{0}_{1:02d}.pdf".format(y, x)
            url5 = "https://jungle.world/system/files/ausgaben/{0}/jungle_world-{0}-{1:02d}.pdf".format(y, x)
            print(url1)
            driver.get(url1)
            time.sleep(2)
            print(url2)
            driver.get(url2)
            time.sleep(2)
            print(url3)
            driver.get(url3)
            time.sleep(2)
            print(url4)
            driver.get(url4)
            time.sleep(2)
            print(url5)
            driver.get(url5)
            time.sleep(2)
    driver.quit()

if __name__ == "__main__":
    from selenium import webdriver
    from selenium.webdriver.chrome.options import Options
    import time
    import subprocess
    import sys
    import os
    main()
